<!DOCTYPE html>
<html>
<head>
	<title>Daily Bug</title>
	 <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
	<div class="d-flex vh-100 ">
		<div class="d-flex vh-100 w-50 align-items-center justify-content-end bg-dark">
			<h1 class="text-secondary" style="font-size: 4rem">DAILY</h1>
		</div>
		<div class="d-flex vh-100 w-50 align-items-center bg-secondary">
			<h1 class="text-dark" style="font-size: 4rem">BUG</h1>
		</div>
	</div>
</body>
</html>