@extends("layouts.app")
@section("content")
<h1 class="text-center py-5">MY BUG'S LIFE</h1>

<div class="container">
	<div class="row">
		@foreach($bugs as $indiv_bug)
		<div class="col-lg-4 my-2 text-center">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">{{$indiv_bug->title}}</h4>
					<p class="card-text">{{$indiv_bug->body}}</p>
					<p class="card-text">{{$indiv_bug->category->name}}</p>
					<p class="card-text">{{$indiv_bug->status->name}}</p>
					<p class="card-text">{{$indiv_bug->user->name}}</p>
				</div>
				<div class="card-footer">
					<form action="/deletebug/{{$indiv_bug->id}}" method="POST">
						@csrf
						@method('DELETE')
						<button class="btn btn-danger btn-block" type="submit">Delete</button>
					</form>
					<a href="/editbug/{{$indiv_bug->id}}" class="btn btn-success btn-block">Edit</a>
					<a href="/indivbug/{{$indiv_bug->id}}" class="btn btn-primary btn-block">Show Details</a>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>	

@endsection